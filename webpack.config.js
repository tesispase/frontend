const path = require('path')
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const devMode = process.env.NODE_ENV !== 'production'
module.exports = {
    entry : './frontendtest/app.js',
    mode: 'development',
    output: {
        path: path.join(__dirname, 'frontendtest/dist'),
        filename: 'bundle.js'
    },
    module : {
        rules: [
          {
            test: /\.css/,
            use: [
              devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
              'css-loader'
            ]
          }
        ]
      },
    plugins:[
        new HtmlWebPackPlugin({
            template: './frontendtest/index.html',
            minify: {
                collapseWhitespace: true,
                removeComments: true,
                removeRedundantAttributes: true,
                removeScriptTypeAttributes: true,
                removeStyleLinkTypeAttributes: true,
                useShortDoctype: true
              }
        }),
        new MiniCssExtractPlugin({
            filename: "css/bundle.css"
          })
    ],
    devtool: 'source-map'
};