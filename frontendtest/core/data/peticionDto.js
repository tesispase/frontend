class peticionDto {
  constructor() {
    this.idProduct = "";
    this.nameProduct = "";
    this.priceInitial = "";
    this.priceFinal = "";
    this.discount = "";
    this.idCategory = "";
    this.nameCategory = "";
    this.page = 0;
    this.numRow = 10;
  }

  response() {
    const data = {
      idProduct: this.idProduct,
      nameProduct: this.nameProduct,
      priceInitial: this.priceInitial,
      priceFinal: this.priceFinal,
      discount: this.discount,
      idCategory: this.idCategory,
      nameCategory: this.nameCategory,
      page: 0,
      numRow: 57,
    };
    return JSON.stringify(data);
  }
}

export default peticionDto;
