require("./styles/app.css");
import UI from "./UI.js";
const items = document.getElementById("contenedorProducto");
const filtro = document.getElementById("filtro");
const search = document.getElementById("search");
const ui = new UI();
document.addEventListener("DOMContentLoaded", () => {
  ui.renderCategorias();
  ui.renderProductos("", "", "");
});
filtro.addEventListener("click", (e) => {
  let descuento;
  let categoria;
  let nombreProducto;
  if (document.getElementById("descuento").checked) {
    descuento = "Descuento";
  }
  categoria = document.getElementById("lstCategoria").value;
  nombreProducto = document.getElementById("nameProducto").value;
  ui.renderProductos(descuento, categoria, nombreProducto);
});
search.addEventListener("click", (e) => {
  let nombreProducto;
  nombreProducto = document.getElementById("inputSearch").value;
  ui.renderProductos("", "", nombreProducto);
});
