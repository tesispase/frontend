import peticionDto from "./core/data/peticionDto";
const peticion = new peticionDto();

const URI = "http://localhost:9091/";
const $select = document.querySelector("#lstCategoria");
const listaProductos = document.querySelector("#contenedorProducto");
class UI {
  renderCategorias() {
    try {
      var request = new Request("https://immense-cliffs-85467.herokuapp.com/Products/categories", {
        method: "GET",
        headers: new Headers(),
      });
      request.headers.append("Content-Type", "application/json");
      fetch(request)
        .then((response) => response.json())
        .then((data) => {
          data.payload.forEach((categoria) => {
            let option = document.createElement("option");
            option.value = categoria.id;
            option.text = categoria.name;
            $select.appendChild(option);
          });
        });
    } catch (err) {
      console.log("fetch failed", err);
    }
  }

  renderProductos(descuento, categoria, producto) {
    peticion.discount = descuento;
    peticion.idCategory = categoria;
    peticion.nameProduct = producto;
    try {
      var request = new Request("https://immense-cliffs-85467.herokuapp.com/Products/products", {
        method: "POST",
        body: peticion.response(),
        headers: {
          "Content-Type": "application/json",
        },
      });
      let template = "";

      fetch(request)
        .then((response) => response.json())
        .then((data) => {
          data.payload.products.forEach((producto) => {
            let img = "";
            if (producto.url_image !== "") {
              img = producto.url_image;
            } else {
              img =
                "https://firebasestorage.googleapis.com/v0/b/bdtecnolamina.appspot.com/o/3aabe0e9a520b9ad90407a82f85adb42.jpg?alt=media&token=14cbba8f-bc1f-438e-971d-b0ec9dfabc5d";
            }
            template += `
            <div class="card producto" >
            <div class="">
                <img src="${img}"
                    class="card-img-top imgProducto">
                <div class="card-body">
                    <h5 class="card-title">${producto.name}</h5>`;
            if (producto.discount != 0) {
              template += ` <span class="badge badge-primary">${producto.discount}% dto.</span>`;
            }
            template += ` 
            <p>$ ${producto.price}</p>
            <button class="btn btn-success btn-block btn-agregar">Agregar</button>
                </div>
            </div>
            </div>
            `;
          });
          if (data.payload.products.length === 0) {
            template += ` <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">No se encontraron registros con los filtros aplicados</h4>
                     </div>`;
          }
          listaProductos.innerHTML = template;
        });
    } catch (err) {
      console.log("fetch failed", err);
    }
  }
}

export default UI;
